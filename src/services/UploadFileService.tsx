import axios from "axios";

const uploadImageApi = axios.create({
  baseURL: "https://upload-file.fly.dev/",
  timeout: 60000, //one minute timeout
  headers: { "Content-Type": "multipart/form-data" },
});

export const uploadImage = async (file: File) => {
  const response = await uploadImageApi.post("aws-s3/upload-image", {
    file: file,
  });
  return response.data;
};
