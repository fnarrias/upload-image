import "./App.css";
import Home from "./pages/Home";
import Upload from "./pages/Upload";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Uploading from "./pages/Uploading";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();
function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home Component={Upload} />} />
          <Route path="/uploading" element={<Uploading />} />
        </Routes>
      </BrowserRouter>
    </QueryClientProvider>
  );
}

export default App;
