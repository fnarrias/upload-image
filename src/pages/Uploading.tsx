import { useQuery } from "react-query";
import { uploadImage } from "../services/UploadFileService";
import { useLocation } from "react-router-dom";
import Loading from "../components/loading/Loading";
import UploadFailed from "../components/upload_status/UploadFailed";
import UploadSuccess from "../components/upload_status/UploadSuccess";

function Uploading() {
  const location = useLocation();
  const postQuery = useQuery({
    queryFn: () => uploadImage(location.state.fileObj),
    queryKey: location.state.fileObj.name,
    staleTime: Infinity,
  });

  if (postQuery.isLoading) {
    return <Loading />;
  }
  if (postQuery.isError) {
    return <UploadFailed></UploadFailed>;
  }
  return (
    <UploadSuccess url={postQuery.data.imageUrl as string}></UploadSuccess>
  );
}

export default Uploading;
