import Card from "@mui/material/Card";
import DragAndUpload from "../components/drag_upload/DragAndUpload";
import UploadButton from "../components/upload_button/UploadButton";

function Upload() {
  return (
    <Card sx={{ padding: "2%" }}>
      <h1>Upload your image</h1>
      <p>File should be jpeg, png ...</p>
      <DragAndUpload></DragAndUpload>
      <p>Or</p>
      <UploadButton></UploadButton>
    </Card>
  );
}

export default Upload;
