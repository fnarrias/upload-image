import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import failRequest from "./assets/failRequest.jpeg";
import { useNavigate } from "react-router-dom";
function UploadFailed() {
  const navigate = useNavigate();
  return (
    <Card sx={{ width: "60%" }}>
      <CardMedia component="img" image={failRequest} alt="Request Failed" />
      <CardContent>
        <Typography variant="h6" color="text.secondary">
          Lo siento, no se pudo subir la imagen, se alcanzó el limite de 5
          imagenes en 1 minuto, el tipo de archivo no corresponde o algun otro
          fallo. :/
        </Typography>
      </CardContent>
      <Link href="#" onClick={() => navigate("/")}>
        Back Home
      </Link>
    </Card>
  );
}

export default UploadFailed;
