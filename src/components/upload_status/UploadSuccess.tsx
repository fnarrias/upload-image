import Card from "@mui/material/Card";
import Link from "@mui/material/Link";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import successRequest from "./assets/successRequest.jpeg";
import { useNavigate } from "react-router-dom";

type Props = {
  url: string;
};
function UploadSuccess(props: Props) {
  const navigate = useNavigate();
  return (
    <Card sx={{ width: "60%" }}>
      <CardMedia component="img" image={successRequest} alt="Success Request" />
      <CardContent>
        <Typography variant="h6" color="text.secondary">
          Éxito! puedes encontrar tu imagen en el siguiente link:
        </Typography>
        <Link href={props.url}>{props.url}</Link>
      </CardContent>
      <Link href="#" onClick={() => navigate("/")}>
        Back home
      </Link>
    </Card>
  );
}

export default UploadSuccess;
