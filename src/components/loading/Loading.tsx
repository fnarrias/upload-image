import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import LinearProgress from "@mui/material/LinearProgress";

function Loading() {
  return (
    <>
      <Card
        sx={{
          padding: "5%",
          minWidth: "50%",
        }}
      >
        <Typography variant="h4" gutterBottom>
          Uploading
        </Typography>
        <LinearProgress />
      </Card>
    </>
  );
}

export default Loading;
