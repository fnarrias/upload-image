import { useCallback } from "react";
import { useDropzone, FileRejection } from "react-dropzone";
import uploadImage from "./assets/uploadImage.jpeg";
import Paper from "@mui/material/Paper";
import { useNavigate } from "react-router-dom";
function DragAndUpload() {
  const navigate = useNavigate();
  const onDrop = useCallback(
    (acceptedFiles: File[], fileRejections: FileRejection[]) => {
      console.log(fileRejections);
      const fileObj = acceptedFiles[0];
      navigate("/uploading", { state: { fileObj } });
    },
    []
  );
  const { getRootProps } = useDropzone({
    onDrop,
    maxFiles: 1,
  });
  return (
    <Paper
      elevation={0}
      sx={{
        backgroundColor: "#f6f8fb",
        border: "1px dashed black",
        padding: "2%",
      }}
      {...getRootProps()}
    >
      <img src={uploadImage} style={{ width: "20%" }} alt="upload image"></img>
      <p>Drag and Drop your image here</p>
    </Paper>
  );
}

export default DragAndUpload;
