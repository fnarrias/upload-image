import Button from "@mui/material/Button";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
function UploadButton() {
  const navigate = useNavigate();
  const inputRef = useRef<HTMLInputElement>(null);

  const handleClick = () => {
    if (inputRef.current != null) {
      inputRef.current.click();
    }
  };

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let fileObj: File;
    if (event.target.files) {
      fileObj = event.target.files[0];
    } else {
      return;
    }
    event.target.value = "";
    navigate("/uploading", { state: { fileObj } });
  };
  return (
    <>
      <input
        style={{ display: "none" }}
        ref={inputRef}
        type="file"
        onChange={handleFileChange}
      ></input>
      <Button
        onClick={handleClick}
        variant="contained"
        size="medium"
        sx={{ textTransform: "none" }}
      >
        Choose a file
      </Button>
    </>
  );
}

export default UploadButton;
